import { useEffect, useState, useContext } from 'react'
import { Form, Button } from 'react-bootstrap'
import { Navigate } from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

export default function Login() {
	const { user, setUser } = useContext(UserContext)
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [isActive, setIsActive] = useState(false)

	useEffect(() => {
		if (email && password) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password])

	const loginUser = (e) => {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-type': 'application/json',
			},
			body: JSON.stringify({
				email: email,
				password: password,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				console.log(data)
				console.log(data.access)

				if (typeof data.access !== 'undefined') {
					localStorage.setItem('token', data.access)
					retrieveUserDetails(data.access)

					Swal.fire({
						title: 'Login Successful',
						icon: 'success',
						text: 'Welcome to Zuitt!',
					})
				} else {
					Swal.fire({
						title: 'Authentication Failed',
						icon: 'error',
						text: 'Please, check your login details and try again',
					})
				}
			})

		// localStorage.setItem('email', email)
		// setUser({ email: localStorage.getItem('email') })

		setEmail('')
		setPassword('')

		console.log(`${email} has been verified. Welcome!`)
	}

	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`,
			},
		})
			.then((res) => res.json())
			.then((data) => {
				console.log(data)
				setUser({
					id: data._id,
					isAdmin: data.isAdmin,
				})
			})
	}

	return user.id ? (
		<Navigate to="/courses" />
	) : (
		<Form onSubmit={(e) => loginUser(e)}>
			<h2>Login</h2>
			<Form.Group className="mb-3" controlId="formBasicEmail">
				<Form.Label>Email address</Form.Label>
				<Form.Control
					type="email"
					value={email}
					onChange={(e) => setEmail(e.target.value)}
					placeholder="Enter Email"
				/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="formBasicPassword">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					value={password}
					onChange={(e) => setPassword(e.target.value)}
					placeholder="Enter Password"
				/>
			</Form.Group>
			<Button variant="success" type="submit" disabled={!isActive}>
				Login
			</Button>
		</Form>
	)
}
