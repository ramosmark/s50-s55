import { useContext } from 'react'
import { Navbar, Nav, Container } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import UserContext from '../UserContext'

export default function AppNavbar() {
	const { user, setUser } = useContext(UserContext)

	// const [user, setUser] = useState(localStorage.getItem('email'))
	console.log(user)

	return (
		<Navbar bg="light" expand="lg">
			<Container fluid>
				<Navbar.Brand as={Link} to="/">
					Zuitt
				</Navbar.Brand>
				<Navbar.Toggle aria-controls="navbarScroll" />
				<Navbar.Collapse id="navbarScroll">
					<Nav className="ml-auto">
						<Nav.Link as={Link} to="/">
							Home
						</Nav.Link>
						<Nav.Link as={Link} to="/courses">
							Courses
						</Nav.Link>
						{user.id ? (
							<Nav.Link as={Link} to="/logout">
								Logout
							</Nav.Link>
						) : (
							<>
								<Nav.Link as={Link} to="/login">
									Login
								</Nav.Link>
								<Nav.Link as={Link} to="/register">
									Register
								</Nav.Link>
							</>
						)}
					</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>
	)
}
