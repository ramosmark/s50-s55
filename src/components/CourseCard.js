import { useState, useEffect } from 'react'
import { Card, Button, Row, Col } from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function CourseCard({ courseProp }) {
	const { name, description, price, _id } = courseProp
	// const [count, setCount] = useState(0)
	// const [seats, setSeat] = useState(30)

	// useEffect(() => {
	// 	if (seats === 0) {
	// 		alert('No more seats vailable')
	// 	}
	// }, [seats])

	return (
		<Row className="my-3">
			<Col xs={12} md={6}>
				<Card>
					<Card.Body>
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>{price}</Card.Text>
						<Button className="bg-primary" as={Link} to={`/courses/${_id}`}>
							Details
						</Button>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}
