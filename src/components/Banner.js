import { Row, Col, Button } from 'react-bootstrap'
import { useLocation } from 'react-router-dom'

export default function Banner() {
	const location = useLocation()

	return (
		<Row>
			<Col className="p-5">
				{location.pathname === '/' ? (
					<>
						<h1>Zuitt Coding Bootcamp</h1>
						<p>Opportunities for everyone, everywhere.</p>
						<Button variant="primary">Enroll Now!</Button>
					</>
				) : (
					<>
						<h1>Page Not Found</h1>
						<p>
							Go back to the <a href="/">Homepage.</a>
						</p>
					</>
				)}
			</Col>
		</Row>
	)
}
